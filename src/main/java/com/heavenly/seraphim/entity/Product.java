package com.heavenly.seraphim.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;


@Entity
@Builder
@Table(name = "product")
public class Product {

    @Id
    @Getter
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name = "specifics")
    private String specifics;

    @Getter
    @Column(name = "productName")
    private String productName;

    @EqualsAndHashCode.Exclude
    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinColumn(name = "brandId")
    private Brand brand;

    @EqualsAndHashCode.Exclude
    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinColumn(name = "baseUnitId")
    private Unit baseUnit;


}
