package com.heavenly.seraphim.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Builder
@Getter
@Setter
@Table(name = "purchaseCostHistory")
public class PurchaseCostHistory {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EqualsAndHashCode.Exclude
    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinColumn(name = "productId")
    private Product product;

    @Column(name = "baseUnitCost")
    private Float baseUnitCost;

    @Column(name = "transactionDate")
    private Timestamp transactionDate;

    @Column(name = "createdBy")
    private String createdBy;
}
